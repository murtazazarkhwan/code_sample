import { Component } from '@angular/core';
import { Map, latLng, tileLayer, Layer, marker, leaflet } from 'leaflet';
import { AlertController } from '@ionic/angular';
import { ApiServiceService } from '../services/api-service.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
declare var L: any;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  layerId: any;
  pageName: string;
  map: Map;
  constructor(
    private alertController: AlertController,
    private apiservice: ApiServiceService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    // this.leafletMap();
    // console.log('calling markers');
    // this.apiservice.getMarkers(this.layerId).then(bds => {
    //   this.addMarkers(bds, this.layerId);
    // });
  }

  ngOnInit() {
    console.log('hello ShowPage')
    // getting the gift id from the url
    this.layerId = this.route.snapshot.paramMap.get('id');
    console.log('ID in params : ', this.layerId);
    if ( this.layerId == 1 ) {
      this.pageName = 'first layer';
    } else if ( this.layerId == 2 ){
      this.pageName = 'second layer';
    } else if ( this.layerId == 3 ){
      this.pageName = 'third layer';
    } else {
      this.pageName = 'fourth layer';
    }
  }

  ionViewWillEnter() {
    // this.leafletMap();
    // console.log('calling markers');
    // this.apiservice.getMarkers(this.layerId).then(bds => {
    //   this.addMarkers(bds, this.layerId);
    // });
  }

  ionViewDidEnter() {
    this.leafletMap();
    console.log('calling markers');
    this.addMarkersToMap();
    // this.apiservice.getMarkers(this.layerId).then(bds => {
    //   this.addMarkers(bds, this.layerId);
    // });
  }


  leafletMap() {

    const id = 'mapId' + this.layerId;
    var map = L.map(id, {
      minZoom: 0,
      maxZoom: 4,
      zoom: 0,
      crs: L.CRS.Simple

    });
    this.map = map;
    // temperoray code should be removed when pages are made dianamic
    var bounds = [[0, 0], [550,850]];
    if ( this.layerId == 2 ) {
      var image = L.imageOverlay("/assets/map1.resized.png", bounds); //initial size ( at zoom 0, double size at zoom 1 )
    } else {
      var image = L.imageOverlay("/assets/map2.png", bounds); //initial size ( at zoom 0, double size at zoom 1 )
    }
    // var image = L.imageOverlay("/assets/metro.png", bounds); //initial size ( at zoom 0, double size at zoom 1 )
    image.addTo(map);
    map.fitBounds(bounds);
    // tell leaflet that the map is exactly as big as the image
    // map.setMaxBounds(new L.LatLngBounds([0, 0], [350, 600])); // prevent panning outside the image area 
    L.tileLayer('', {
      attribution: '&copy; <a href="#">MetsTech</a>'
    }).addTo(map);
    map.on('click', (e)=>{this.presentAlertPrompt(e)});
  }

  /** Remove map when we have multiple map object */
  ionViewWillLeave() {
    this.map.remove();
  }


  addMarkerscc(bds, id) {
    console.log('inside addMarkers');
    console.log('bdas', bds.bdas);
    for (let bda of bds.bdas) {
      L.marker([bda.latitude, bda.longitude]).addTo(this.map).bindPopup(bda.name, {autoClose: false, closeOnClick: false});
    }
  }
  addMarkers(bds, id) {
    console.log('inside addMarkers', bds);
    var LeafIcon = L.Icon.extend({
      options: {
         iconSize:     [35, 35]
      }
    });
    var greenIcon = new LeafIcon({
      iconUrl: '/assets/mark.png'
    });

    // if ( this.layerId == 2 ) {
    //   var markerArray = [[87, 301.62358875017907], [206, 90.50457636972087], [244, 512.3099127086615], [320, 763.1941293462778]]

    //   for (let bda of markerArray ) {
    //     L.marker([bda[0], bda[1]], {icon: greenIcon})
    //     .addTo(this.map)
    //     .bindPopup('BDA', {autoClose: false, closeOnClick: false});
    //   }
    // } else if ( this.layerId == 1 ) {
    //   var markerArray = [[157, 77.51057311757341], [20, 232.43907343163926], [156, 516.3080675554761], [380, 696.2273421036149]]

    // for (let bda of bds.bdas ) {
    //   L.marker([bda.latitude, bda.longitude], {icon: greenIcon})
    //   .addTo(this.map)
    //   .bindPopup('BDA', {autoClose: false, closeOnClick: false});
    // }
    // } else {



    // these are from pi

    for (let bda of bds.bdas ) {
      if (bda != null) {
        L.marker([bda.latitude, bda.longitude], {icon: greenIcon})
        .addTo(this.map)
        .bindPopup('BDA', { autoClose: false, closeOnClick: false });
      }
    }
  }

  onMapClick(e) {
    console.log('e', e);
  }

  addMarkersToMap(){
    this.apiservice.getMarkers(this.layerId).then(bds => {
      this.addMarkers(bds, this.layerId);
    });
  }

  addToMap(latlng, form) {
    this.apiservice.createMarker(form).then(res => {
      console.log('response from api method call', res);
      this.addMarkersToMap();
    });
  }

  async presentAlertPrompt(latLng) {
    let alert = await this.alertController.create({
      header: 'Add Marker',
      inputs: [
        {
          name: 'latitude',
          type: 'text',
          placeholder: 'Latitude',
          value: latLng.latlng.lat
        },
        {
          name: 'longitude',
          type: 'text',
          id: 'name2-id',
          value: latLng.latlng.lng,
          placeholder: 'Longitude'
        },
        {
          name: 'name',
          value: '',
          type: 'url',
          placeholder: 'Name'
        },
        // input date with min & max
        {
          name: 'level',
          type: 'text',
          placeholder: 'Level'
        },
        {
          name: 'layer_id',
          type: 'text',
          placeholder: this.layerId,
          value: this.layerId,
          disabled: true
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Add',
          handler: (alertData) => {
            console.log('Confirm Ok');
            this.addToMap(latLng.latlng, alertData);
          }
        }
      ]
    });
    await alert.present();
  }
}
