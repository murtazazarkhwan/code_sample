'use strict';
/*global APP */
/*global _ */
/*jslint node:true */

angular.module(APP)

.controller('wkJobListController', [
  'wk', '$log', '$scope', 'jobsApi', 'widgetSettingsService', 'messageService', '$sce', function (
  wk, $log, $scope, jobsApi, widgetSettingsService, messageService, $sce) {

    $scope.model = {
      title: null,
      error: null,
      errorMessage: null,
      template: {
        listItem: '<%= job.name %>'
      },
      query: {},
      jobs: null
    };

    function defaults(isError, errorMessage) {
      $scope.model.title = $scope.model.title || null;
      $scope.model.error = isError;
      $scope.model.errorMessage = errorMessage;
    }

    $scope.format = function (job) {
      var template = $scope.model.template.listItem;
      var templateHtml = _.template(template)({ job: job });
      return $sce.trustAsHtml(templateHtml);
    };

    // Overlay the incoming widget settings with defaults, if not supplied by the person configuring the system
    function defaultSettings(settings) {

      if (settings.error.isError) {
        var message = _.first(settings.error.messages);
        defaults(true, message);

        //throw settings.error.messages;
        throw new wk.WkError(message);
      }

      settings.config = settings.config || {};
      settings.config.title = settings.config.title || '';
      settings.config.template = settings.config.template || $scope.model.template;

      return settings;
    }

    function mapSettingsToScope(settings) {
      $scope.model.title = settings.config.title;
      $scope.model.template = settings.config.template || $scope.model.template;
      $scope.model.query = settings.query;
      $scope.model.query.layout = 'detailed';
      $scope.model.powered_title = settings.config.powered_title || 'JobGetter';
      $scope.model.powered_url = settings.config.powered_url || 'https://my.jobgetter.com/';
      $scope.model.powered_target = settings.config.powered_target || 'jobgetter';
    }

    // Expecting an array of error messages or a single string error message or an exception message
    // Refactor: This code needs to be in a library function maybe called parseError
    function errorHandler(error) {

      var messages = [];

      if (_.isString(error)) {
        messages.push(error);
      }

      if (error instanceof Error) {
        messages.push(error.message);
        $log.error(error.stack);
      }

      if (_.isArray(error)) {
        messages = messages.concat(error);
      }

      messages.push('Id: ' + $scope.settingsId);
      messageService.addError(messages, 'Job List Configuration Error');
    }

    function search() {

      // c_onsole.log(JSON.stringify($scope.model.query));

      jobsApi.feed({ options: $scope.model.query }, function (data) {

        $scope.model.jobs = data.rows;

        for (var i = 0; i < $scope.model.jobs.length; i++) {
          var job = $scope.model.jobs[i];

          job.titleize = {
            work_type: _.map(job.work_types, titleize).join(', ')
          };
        }

        defaults(false, '');

      }, function (error) {
        $log.error(error);

        defaults(true, 'Job List loading error');
      });
    }

    function titleize(value) {
      return (value === null) ? '' : _.titleize(value.replace('_', ' '));
    }

    // Init Code
    // Must be called after all functions are created (e.g. at the end of the file)
    widgetSettingsService
      .$get($scope.settingsId)
      .then(defaultSettings)
      .then(mapSettingsToScope)
      .then(search)
      .catch(errorHandler);

  }]);
