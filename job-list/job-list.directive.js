'use strict';
/*global APP */
/*jslint node:true */

angular.module(APP)

.directive('wkJobList', [function () {
  return {
    restrict: 'EA',
    templateUrl: 'app/widgets/job-list/job-list.html',
    scope: {
      settingsId: '@'
    },
    controller: 'wkJobListController'
  };
}]);